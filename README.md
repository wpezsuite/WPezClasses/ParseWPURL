## WPezClasses: Parse WP URL

__A handy combination of wp_upload_dir() meets parse_url() for parsing WordPress (attachment) URLs.__

Please see the __get() method for the list of URL attributes available.   

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

1) Instantiate.

2) Use the setURL() method to set your URL.

3) Now __get() what you need.


### Tips & Tricks


    $new = new ClassParseWPURL()
    $new->setULR('https://my-url.com');
    
    $foo[] = $new->abspath;
    $foo[] = $new->uploads;
    $foo[] = $new->uploads_subdir;
    
    // since we know nothing leads or ends with a slash we can 
    // use implode for making strings
    $bar = implode('/', $foo);



### FAQ

__1) Why?__

Too many wasted searches and keystrokes reinventing the wheel while trying to do some basic path and URL manipulations.


__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __#3 - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

 - https://developer.wordpress.org/reference/functions/wp_upload_dir/


 - https://www.php.net/manual/en/function.parse-url.php
 

### TODO

- Check for compatibility with WP Multisite, as well as the wp_config.php UPLOADS constant being defined in non-traditional ways. 


### CHANGE LOG

- v0.0.4 - 18 April 2019
    - UPDATED: namespace

- v0.0.3 - 1 April 2019
    - ADDED - individual getters 


- v0.0.2 - 31 March 2019
    - Hey! Ho!! Let's go!!! 